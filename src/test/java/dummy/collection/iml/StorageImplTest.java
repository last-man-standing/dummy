package dummy.collection.iml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Iterator;
import java.util.function.Consumer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import dummy.collection.Storage;
import dummy.collection.impl.StorageImpl;

class StorageImplTest
{
    private Consumer<Integer> consumer;

    @AfterEach
    public void afterEach()
    {
        verifyNoMoreInteractions(consumer);
    }

    @BeforeEach
    @SuppressWarnings("unchecked")
    public void beforEach()
    {
        consumer = Mockito.mock(Consumer.class);
    }

    @Test
    void testForEachShouldOkWhenStorageIsEmpty()
    {
        Storage<Integer> storage = new StorageImpl<>(0);
        storage.forEach(consumer);
    }

    @Test
    void testGetShouldReturnCorrectValueWhenIndexLast()
    {
        Storage<Integer> storage = new StorageImpl<>(5);
        storage.set(storage.size() - 1, 15);
        assertEquals(15, storage.get(storage.size() - 1));
    }

    @Test
    void testGetShouldReturnCorrectValueWhenIndexMiddle()
    {
        Storage<Integer> storage = new StorageImpl<>(5);
        storage.set(3, 15);
        assertEquals(15, storage.get(3));
    }

    @Test
    void testGetShouldReturnCorrectValueWhenIndexZero()
    {
        Storage<Integer> storage = new StorageImpl<>(5);
        storage.set(0, 15);
        assertEquals(15, storage.get(0));
    }

    @Test
    @SuppressWarnings("boxing")
    void testIteratorShouldOkWhenStorageHaveSeveralElements()
    {
        Storage<Integer> storage = new StorageImpl<>(15);
        for (int i = 0; i < 15; i++)
        {
            storage.set(i, i - 15);
        }
        Iterator<Integer> iterator = storage.iterator();
        while (iterator.hasNext())
        {
            consumer.accept(iterator.next());
        }
        for (int i = 0; i < 15; i++)
        {
            verify(consumer, times(1)).accept(i - 15);
        }
    }

    @Test
    void testIteratorShouldOkWhenStorageIsEmpty()
    {
        Storage<Integer> storage = new StorageImpl<>(0);
        Iterator<Integer> iterator = storage.iterator();
        while (iterator.hasNext())
        {
            consumer.accept(iterator.next());
        }
    }

    @Test
    void testSizeShouldMatchSpecified()
    {
        Storage<Integer> storage = new StorageImpl<>(5);
        assertEquals(5, storage.size());
    }

}
