package dummy.collection;

/**
 * Generic storage interface
 * 
 * @param <T>
 *            - identifies type of the data elements
 */
public interface Storage<T> extends Iterable<T>
{
    /**
     * Get value by index. Returns null if index is not valid.
     * 
     * @param index
     * @return value or null
     */
    T get(int index);

    /**
     * Set value by index. Does nothing if index is not valid.
     * 
     * @param index
     * @param value
     * @return true when set or false if not
     */
    boolean set(int index, T value);

    /**
     * Return size of the storage
     * 
     * @return
     */
    int size();
}
