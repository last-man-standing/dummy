package dummy.collection.impl;

import java.util.ArrayList;
import java.util.Iterator;

import dummy.collection.Storage;

public class StorageImpl<T> implements Storage<T>
{

    private final ArrayList<T> storage;

     /**
     * Create new storage with initial size
     * 
     */
    public StorageImpl(int size)
    {
        storage = new ArrayList<T>();

        for (int i = 0; i < size; i++)
            storage.add(null);
    }

    @Override
    public T get(int index)
    {
        if (index < storage.size() && index >= 0)
        {
            T getValue = storage.get(index);
            return getValue;
        }
        else
            return null;
    }

    @Override
    public Iterator<T> iterator()
    {
        Iterator<T> iterator = storage.iterator();
        return iterator;
    }

    @Override
    public boolean set(int index, T value)
    {
        if (index < storage.size() && index >= 0)
        {
            storage.set(index, value);
            return true;
        }
        else
            return false;
    }

    @Override
    public int size()
    {
        return storage.size();
    }
}
