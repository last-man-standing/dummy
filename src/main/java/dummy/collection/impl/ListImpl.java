package dummy.collection.impl;

import java.util.Iterator;

import dummy.collection.List;
import dummy.collection.Storage;

public class ListImpl<T> implements List<T>
{

    /**
     * {@inheritDoc}
     * 
     * @see dummy.collection.List#add(dummy.collection.List, int, boolean)
     */
    @Override
    public boolean add(List<T> values, int index, boolean after)
    {
        // TODO implement add
        return false;
    }

    /**
     * {@inheritDoc}
     * 
     * @see dummy.collection.List#add(dummy.collection.Storage, int, boolean)
     */
    @Override
    public boolean add(Storage<T> values, int index, boolean after)
    {
        // TODO implement add
        return false;
    }

    @Override
    public boolean add(T value, int index, boolean after)
    {
        // TODO implement add
        return false;
    }

    @Override
    public int capacity()
    {
        // TODO implement capacity
        return 0;
    }

    @Override
    public T get(int index)
    {
        // TODO implement get
        return null;
    }

    @Override
    public Iterator<T> iterator()
    {
        // TODO implement iterator
        return null;
    }

    @Override
    public boolean reduce()
    {
        // TODO implement reduce
        return false;
    }

    @Override
    public boolean set(int index, T value)
    {
        // TODO implement set
        return false;
    }

    @Override
    public int size()
    {
        // TODO implement size
        return 0;
    }

}
