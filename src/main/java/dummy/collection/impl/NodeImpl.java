package dummy.collection.impl;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;

import dummy.collection.Node;

public class NodeImpl<T> implements Node<T>
{
    @Override
    public boolean add(Node<T> node, int index, boolean after)
    {
        // TODO implement add
        return false;
    }

    @Override
    public boolean add(T value, int index, boolean after)
    {
        // TODO implement add
        return false;
    }

    /**
     * {@inheritDoc}
     * 
     * @see dummy.collection.Node#get()
     */
    @Override
    public T get()
    {
        // TODO implement get
        return null;
    }

    @Override
    public Optional<Node<T>> getParent()
    {
        // TODO implement getParent
        return null;
    }

    @Override
    public Iterator<T> iterator()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see dummy.collection.Node#set(java.lang.Object)
     */
    @Override
    public void set(T value)
    {
        // TODO implement set

    }

    @Override
    public int size()
    {
        return 0;
    }

    @Override
    public void traverseNodes(Consumer<Node<T>> nodeConsumer, boolean currentFirst)
    {
        // TODO implement traverseNodes

    }

    @Override
    public void traverseValues(Consumer<T> valueConsumer, boolean currentFirst)
    {
        // TODO implement traverseValues

    }

}
