package dummy.collection;

/**
 * Generic list interface
 *
 * @param <T>
 *            type of the data elements
 */
public interface List<T> extends Storage<T>
{
    /**
     * Add values to the list. Values added to the end of the list.
     * 
     * @param values
     *            to be added
     * @return
     */
    default boolean add(List<T> values)
    {
        return add(values, size() - 1);
    }

    /**
     * Add values to the list after specified index. Values added after
     * specified index.
     * 
     * @param values
     *            to be added
     * @param index
     *            to point position of the elements
     * @return
     */
    default boolean add(List<T> values, int index)
    {
        return add(values, index, true);
    }

    /**
     * Add values to the list after or before specified index (depends on after
     * flag).
     * 
     * @param values
     *            to be added
     * @param index
     *            to point position of the elements
     * @param after
     *            - flag to toggle positioning
     * @return
     */
    boolean add(List<T> values, int index, boolean after);

    /**
     * Add values to the list. Values added to the end of the list.
     * 
     * @param values
     *            to be added
     * @return
     */
    default boolean add(Storage<T> values)
    {
        return add(values, size() - 1);
    }

    /**
     * Add values to the list after specified index. Values added after
     * specified index.
     * 
     * @param values
     *            to be added
     * @param index
     *            to point position of the elements
     * @return
     */
    default boolean add(Storage<T> values, int index)
    {
        return add(values, index, true);
    }

    /**
     * Add values to the list after or before specified index (depends on after
     * flag).
     * 
     * @param values
     *            to be added
     * @param index
     *            to point position of the elements
     * @param after
     *            - flag to toggle positioning
     * @return
     */
    boolean add(Storage<T> values, int index, boolean after);

    /**
     * Add value to the list. Value added to the end of the list.
     * 
     * @param value
     *            to point position of the element
     * @return
     */
    default boolean add(T value)
    {
        return add(value, size() - 1);
    }

    /**
     * Add value to the list after specified index
     * 
     * @param value
     *            to be added
     * @param index
     *            to point position of the element
     * @return
     */
    default boolean add(T value, int index)
    {
        return add(value, index, true);
    }

    /**
     * Add value to the list after or before specified index (depends on after
     * flag).
     * 
     * @param value
     * @param index
     *            to point position of the element
     * @param after
     *            - flag to toggle positioning
     * @return
     */
    boolean add(T value, int index, boolean after);

    /**
     * Returns current capacity
     * 
     * @return
     */
    int capacity();

    /**
     * Trim capacity down to actual size
     * 
     * @return
     */
    boolean reduce();
}
