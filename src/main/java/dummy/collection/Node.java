package dummy.collection;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface Node<T> extends Iterable<T>, Supplier<T>
{
    /**
     * Add value to the list. Value added to the end of the list.
     * 
     * @param node
     *            to be added
     * @return
     */
    default boolean add(Node<T> node)
    {
        return add(node, size() - 1);
    }

    /**
     * Add value to the list after specified index
     * 
     * @param node
     *            to be added
     * @param index
     *            to point position of the element
     * @return
     */
    default boolean add(Node<T> node, int index)
    {
        return add(node, index, true);
    }

    /**
     * Add value to the list after or before specified index (depends on after
     * flag).
     * 
     * @param node
     * @param index
     *            to point position of the element
     * @param after
     *            - flag to toggle positioning
     * @return
     */
    boolean add(Node<T> node, int index, boolean after);

    /**
     * Add value to the list. Value added to the end of the list.
     * 
     * @param value
     *            to point position of the element
     * @return
     */
    default boolean add(T value)
    {
        return add(value, size() - 1);
    }

    /**
     * Add value to the list after specified index
     * 
     * @param value
     *            to be added
     * @param index
     *            to point position of the element
     * @return
     */
    default boolean add(T value, int index)
    {
        return add(value, index, true);
    }

    /**
     * Add value to the list after or before specified index (depends on after
     * flag).
     * 
     * @param value
     * @param index
     *            to point position of the element
     * @param after
     *            - flag to toggle positioning
     * @return
     */
    boolean add(T value, int index, boolean after);

    /**
     * Returns node value
     */
    @Override
    T get();

    /**
     * Return reference to parent node if it has parent node
     * 
     * @return optional of parent node
     */
    Optional<Node<T>> getParent();

    /**
     * Set node value
     * 
     * @param value
     */
    void set(T value);

    /**
     * Return amount of child nodes
     * 
     * @return
     */
    int size();

    /**
     * Iterates over all nodes (e.g. current and all its children) and pass to
     * the consumer. Handles current node first.
     * 
     * @param nodeConsumer
     *            - consumer to handle nodes
     */
    default void traverseNodes(Consumer<Node<T>> nodeConsumer)
    {
        traverseNodes(nodeConsumer, true);
    }

    /**
     * Iterates over all nodes (e.g. current and all its children) and pass to
     * the consumer.
     * 
     * @param valueConsumer
     *            - consumer to handle nodes
     * @param currentFirst
     *            - specify order of handling children comparing to handling
     *            current node
     */
    void traverseNodes(Consumer<Node<T>> nodeConsumer, boolean currentFirst);

    /**
     * Iterates over all nodes (e.g. current and all its children) and pass to
     * the consumer. Handles current node first.
     * 
     * @param valueConsumer
     *            - consumer to handle values
     */
    default void traverseValues(Consumer<T> valueConsumer)
    {
        traverseValues(valueConsumer, true);
    }

    /**
     * Iterates over all nodes (e.g. current and all its children) and pass to
     * the consumer.
     * 
     * @param valueConsumer
     *            - consumer to handle the values
     * @param currentFirst
     *            - specify order of handling children comparing to handling
     *            current node
     */
    void traverseValues(Consumer<T> valueConsumer, boolean currentFirst);
}
