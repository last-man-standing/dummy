package dummy.collection;

/**
 * Extended storage interface
 * 
 * @param <T>
 *            - identifies type of the data elements
 */
public interface StorageEx<T> extends Storage<T>
{
    /**
     * Create new storage of the same size and content
     * 
     * @return new storage instance
     */
    default StorageEx<T> copy()
    {
        return copy(size());
    }

    /**
     * Create new storage with specified size.</br>
     * If specified `size` is less then current size `size()` only specified
     * amount of elements are copied.</br>
     * If specified `size` is greater then current size `size()` only `size()`
     * amount of elements are copied. If specified `size` is not valid (e.g. is
     * not positive number) creates copy of the same size.
     * 
     * @param size
     *            - size of storage to be created
     * @return new storage instance
     */
    StorageEx<T> copy(int size);

    /**
     * Change size of current storage. Does nothing if size is not positive
     * number. Does nothing if size is the same. Preserve content.
     * 
     * @param size
     *            - new size of the storage
     * @return true if resized or false when not
     */
    default boolean resize(int size)
    {
        return resize(size, true);
    }

    /**
     * Change size of current storage. Does nothing if size is not positive
     * number. Does nothing if size is the same.
     * 
     * @param size
     *            - new size of the storage
     * @param preserve
     *            - when true all content will be preserved, false - all content
     *            will be erased.
     * @return true if resized or false when not
     */
    boolean resize(int size, boolean preserve);
}
